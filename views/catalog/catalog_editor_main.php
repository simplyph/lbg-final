<?php    
    if(!current_user_can( 'manage_options' )) {
        return;
    }

    if ( ! function_exists( 'wp_handle_upload' ) ) {
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
    }
    

    require plugin_dir_path( __DIR__ )  . '/../vendor/autoload.php';

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
    use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
?>

<div class="wrap">

    <?php

        // PER LINE
        // Store LBG Number if not unique
        // Get the Interchange
        // Get the OEM
        // Get the Dimensions
        // Create Category, Make, Model, Engine, Year if not unique
        // Store All
        // Repeat

        if (!empty($_POST)) {
            if(!empty($_FILES)) {
                if(isset($_FILES['excelfile'])) {

                    $uploadedfile = $_FILES['excelfile'];
                    $upload_overrides = array( 'test_form' => false );

                    $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
                    $reader = new Xlsx();
                    $spreadsheet = $reader->load($movefile['file']);
                    $worksheet = $spreadsheet->getActiveSheet();
            
                    global $wpdb;
            
                    // Constants
                    $highestRow = $worksheet->getHighestRow();
                    $startRow = 2;
            
                    // Table Names
                    $tableInterchange =     $wpdb->prefix . 'ce_interchanges';
                    $tableOem =             $wpdb->prefix . 'ce_oems';
                    $tableDimension =       $wpdb->prefix . 'ce_dimensions';
                    $tablePart =            $wpdb->prefix . 'ce_parts';
                    $tableCategory =        $wpdb->prefix . 'list_category';
                    $tableLbg =             $wpdb->prefix . 'list_lbg';
                    $tableYear =            $wpdb->prefix . 'list_year';
                    $tableMake =            $wpdb->prefix . 'list_make';
                    $tableModel =           $wpdb->prefix . 'list_model';
                    $tableEngine =          $wpdb->prefix . 'list_engine';

            
                    $wpdb->query('START TRANSACTION');


                    for($row = $startRow; $row <= $highestRow; ++$row) {
                        // Variables
                        $idLbg = NULL;
                        $idInterchange = NULL;
                        $idOem = NULL;
                        $idDimension = NULL;
                        $idCategory = NULL;
                        $idMake = NULL;
                        $idModel = NULL;
                        $idEngine = NULL;
                        $idYear = NULL;

                        // LBG Number
                        $fieldLbg = $worksheet->getCellByColumnAndRow(1, $row)->getValue();

                        $idLbg = $wpdb->get_var($wpdb->prepare(
                            "
                                SELECT id FROM $tableLbg
                                WHERE LOWER(part_name) = %s
                            ",
                            array(
                                $fieldLbg
                            )
                        ));
                        
                        if($idLbg === NULL) {
                            $resultLbg = $wpdb->insert(
                                $tableLbg,
                                array(
                                    'part_name' => $worksheet->getCellByColumnAndRow(1, $row)->getValue()
                                ),
                                array(
                                    '%s'
                                )
                            );

                            if($resultLbg == TRUE) {
                                $idLbg = $wpdb->insert_id;
                            } else {
                                echo "Failed on inserting, Please try again!";                  
                                $wpdb->query('ROLLBACK');
                                break;
                            }
                        }

                        // Interchange
                        $fieldInterchange1 = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $fieldInterchange2 = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $fieldInterchange3 = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                        $fieldInterchange4 = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                        $fieldInterchange5 = $worksheet->getCellByColumnAndRow(6, $row)->getValue();

                        $fieldInterchange1 = trim($fieldInterchange1) !== '' ? trim($fieldInterchange1) : NULL; 
                        $fieldInterchange2 = trim($fieldInterchange2) !== '' ? trim($fieldInterchange2) : NULL; 
                        $fieldInterchange3 = trim($fieldInterchange3) !== '' ? trim($fieldInterchange3) : NULL; 
                        $fieldInterchange4 = trim($fieldInterchange4) !== '' ? trim($fieldInterchange4) : NULL; 
                        $fieldInterchange5 = trim($fieldInterchange5) !== '' ? trim($fieldInterchange5) : NULL; 

                        $resultInterchange = $wpdb->insert(
                            $tableInterchange,
                            array(
                                'gates_aus' => $fieldInterchange1,
                                'gates_usa' => $fieldInterchange2,
                                'dayco_aus' => $fieldInterchange3,
                                'continental' => $fieldInterchange4,
                                'other' => $fieldInterchange5,
                            ),
                            array(
                                '%s',
                                '%s',
                                '%s',
                                '%s',
                                '%s'
                            )
                        );

                        if($resultInterchange == TRUE) {
                            $idInterchange = $wpdb->insert_id;
                        } else {
                            echo "Failed on inserting, Please try again!";                            
                            $wpdb->query('ROLLBACK');
                            break;
                        }

                        // OEMs
                        $fieldOem1 = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                        $fieldOem2 = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                        $fieldOem3 = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                        $fieldOem4 = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                        $fieldOem5 = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                        $fieldOem6 = $worksheet->getCellByColumnAndRow(12, $row)->getValue();

                        $fieldOem1 = trim($fieldOem1) !== '' ? trim($fieldOem1) : NULL; 
                        $fieldOem2 = trim($fieldOem2) !== '' ? trim($fieldOem2) : NULL; 
                        $fieldOem3 = trim($fieldOem3) !== '' ? trim($fieldOem3) : NULL; 
                        $fieldOem4 = trim($fieldOem4) !== '' ? trim($fieldOem4) : NULL; 
                        $fieldOem5 = trim($fieldOem5) !== '' ? trim($fieldOem5) : NULL; 
                        $fieldOem6 = trim($fieldOem6) !== '' ? trim($fieldOem6) : NULL; 

                        $resultOem = $wpdb->query($wpdb->prepare(
                            "
                                INSERT INTO $tableOem
                                (oem1, oem2, oem3, oem4, oem5, oem6)
                                VALUES (%s, %s, %s, %s, %s, %s)
                            ",
                                $fieldOem1,
                                $fieldOem2,
                                $fieldOem3,
                                $fieldOem4,
                                $fieldOem5,
                                $fieldOem6
                            )
                        );

                        if($resultOem == TRUE) {
                            $idOem = $wpdb->insert_id;
                        } else {
                            echo "Failed on inserting, Please try again!";                            
                            $wpdb->query('ROLLBACK');
                            break;
                        }

                        // Dimensions
                        $fieldDimensions1 = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                        $fieldDimensions2 = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                        $fieldDimensions3 = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                        $fieldDimensions4 = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                        $fieldDimensions5 = $worksheet->getCellByColumnAndRow(21, $row)->getValue();

                        $fieldDimensions1 = trim($fieldDimensions1) !== '' ? trim($fieldDimensions1) : NULL; 
                        $fieldDimensions2 = trim($fieldDimensions2) !== '' ? trim($fieldDimensions2) : NULL; 
                        $fieldDimensions3 = trim($fieldDimensions3) !== '' ? trim($fieldDimensions3) : NULL; 
                        $fieldDimensions4 = trim($fieldDimensions4) !== '' ? trim($fieldDimensions4) : NULL; 
                        $fieldDimensions5 = trim($fieldDimensions5) !== '' ? trim($fieldDimensions5) : NULL;

                        $resultDimension = $wpdb->query($wpdb->prepare(
                            "
                                INSERT INTO $tableDimension
                                (inside_diameter_a, inside_diameter_b, inside_diameter_c, inside_diameter_d, centerline_length_a)
                                VALUES (%f, %f, %f, %f, %f)
                            ",
                                $fieldDimensions1,
                                $fieldDimensions2,
                                $fieldDimensions3,
                                $fieldDimensions4,
                                $fieldDimensions5
                            )
                        );

                        if($resultDimension == TRUE) {
                            $idDimension = $wpdb->insert_id;
                        } else {
                            echo "Failed on inserting, Please try again!";                            
                            $wpdb->query('ROLLBACK');
                            break;
                        }


                        // Category
                        $fieldCategory = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
                        $fieldCategory = trim($fieldCategory) !== '' ? trim($fieldCategory) : NULL;

                        $idCategory = $wpdb->get_var($wpdb->prepare(
                            "
                                SELECT id FROM $tableCategory
                                WHERE LOWER(category_name) = %s
                            ",
                            array(
                                $fieldCategory
                            )
                        ));
                        
                        if($idCategory === NULL) {
                            $resultCategory = $wpdb->insert(
                                $tableCategory,
                                array(
                                    'category_name' => $fieldCategory,
                                )
                            );                        

                            if($resultCategory == TRUE) {
                                $idCategory = $wpdb->insert_id;
                            } else {
                                echo "Failed on inserting, Please try again!";                            
                                $wpdb->query('ROLLBACK');
                                break;
                            }
                        }

                        // Make
                        $fieldMake = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
                        $fieldMake = trim($fieldMake) !== '' ? trim($fieldMake) : NULL;

                        $idMake = $wpdb->get_var($wpdb->prepare(
                            "
                                SELECT id FROM $tableMake
                                WHERE LOWER(make_name) = %s
                            ",
                            array(
                                $fieldMake
                            )
                        ));
                        
                        if($idMake === NULL) {                            
                            $resultMake = $wpdb->query($wpdb->prepare(
                                "
                                    INSERT INTO $tableMake
                                    (make_name)
                                    VALUES (%s)
                                ",
                                    $fieldMake
                                )
                            );

                            if($resultMake == TRUE) {
                                $idMake = $wpdb->insert_id;
                            } else {
                                echo "Failed on inserting, Please try again!";                            
                                $wpdb->query('ROLLBACK');
                                break;
                            }
                        }

                        // Model
                        $fieldModel = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
                        $fieldModel = trim($fieldModel) !== '' ? trim($fieldModel) : NULL;

                        $idModel = $wpdb->get_var($wpdb->prepare(
                            "
                                SELECT id FROM $tableModel
                                WHERE LOWER(model_name) = %s
                            ",
                            array(
                                $fieldModel
                            )
                        ));
                        
                        if($idModel === NULL) {                            
                            $resultModel = $wpdb->query($wpdb->prepare(
                                "
                                    INSERT INTO $tableModel
                                    (model_name)
                                    VALUES (%s)
                                ",
                                    $fieldModel
                                )
                            );

                            if($resultModel == TRUE) {
                                $idModel = $wpdb->insert_id;
                            } else {
                                echo "Failed on inserting, Please try again!";                            
                                $wpdb->query('ROLLBACK');
                                break;
                            }
                        }

                        // Engine
                        $fieldEngine = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
                        $fieldEngine = trim($fieldEngine) !== '' ? trim($fieldEngine) : NULL;

                        $idEngine = $wpdb->get_var($wpdb->prepare(
                            "
                                SELECT id FROM $tableEngine
                                WHERE LOWER(engine_name) = %s
                            ",
                            array(
                                $fieldEngine
                            )
                        ));
                        
                        if($idEngine === NULL) {                            
                            $resultEngine = $wpdb->query($wpdb->prepare(
                                "
                                    INSERT INTO $tableEngine
                                    (engine_name)
                                    VALUES (%s)
                                ",
                                    $fieldEngine
                                )
                            );

                            if($resultEngine == TRUE) {
                                $idEngine = $wpdb->insert_id;
                            } else {
                                echo "Failed on inserting, Please try again!";                            
                                $wpdb->query('ROLLBACK');
                                break;
                            }
                        }

                        // Year
                        $fieldYear = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
                        $fieldYear = trim($fieldYear) !== '' ? trim($fieldYear) : NULL;

                        $resultYear = $wpdb->query($wpdb->prepare(
                            "
                                INSERT INTO $tableYear
                                (display_year)
                                VALUES (%s)
                            ",
                                $fieldYear
                            )
                        );

                        if($resultYear == TRUE) {
                            $idYear = $wpdb->insert_id;
                        } else {
                            echo "Failed on inserting, Please try again!";                            
                            $wpdb->query('ROLLBACK');
                            break;
                        }

                        // Main Part
                        $fieldSpring = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
                        $fieldSpring = trim($fieldSpring) !== '' ? trim($fieldSpring) : 'NO';
                        $fieldSpring = $fieldSpring == 'YES' ? 1 : 0;

                        if($idLbg !== NULL && $idInterchange !== NULL && $idOem !== NULL && $idDimension !== NULL && $idCategory !== NULL && 
                        $idMake !== NULL && $idModel !== NULL && $idEngine !== NULL && $idYear !== NULL) {

                            $resultPart = $wpdb->query($wpdb->prepare(
                                "
                                    INSERT INTO $tablePart
                                    (lbg_number, interchange_id, oem_id, dimension_id, category_id, make_id, model_id, engine_id, year_id, is_spring)
                                    VALUES (%d, %d, %d, %d, %d, %d, %d, %d, %d, %d)
                                ",
                                    $idLbg,
                                    $idInterchange,
                                    $idOem,
                                    $idDimension,
                                    $idCategory,
                                    $idMake,
                                    $idModel,
                                    $idEngine,
                                    $idYear,
                                    $fieldSpring
                                )
                            );
    
                            if($resultPart == TRUE) {
                                $idPart = $wpdb->insert_id;
                            } else {
                                echo "Failed on inserting, Please try again!";                            
                                $wpdb->query('ROLLBACK');
                                break;
                            }

                        } else {
                            echo "Failed on inserting, Please try again!";                            
                            $wpdb->query('ROLLBACK');
                            break;
                        }

                    }

                    $wpdb->query('COMMIT');
                }
            }
        }
    ?>
    <h1><?= esc_html(get_admin_page_title()); ?></h1>

    <form enctype="multipart/form-data" action="" method="POST">
        <!-- MAX_FILE_SIZE must precede the file input field -->
        <input type="hidden" name="MAX_FILE_SIZE" value="3000000" />
        Excel File: <input id="excelfile" name="excelfile" type="file" />
        <input type="submit" value="Send File" />
    </form>

</div>

<script>
    (function ($) {
        "use strict";
    })(jQuery);
</script>