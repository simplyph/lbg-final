<?php

/*

Plugin Name: Catalog Editor Plugin
Plugin URI:
Description: Custom catalog plugin
Author: Knowell Cedrich Riego
Author URI:
Version: 0.1

*/

define( 'WPPS_NAME',                 'Catalog Editor Plugin' );
define( 'WPPS_REQUIRED_PHP_VERSION', '5.3' );                          // because of get_called_class()
define( 'WPPS_REQUIRED_WP_VERSION',  '4.0.0' );                          // because of esc_textarea()\
define( 'CE_FILE', __FILE__ );

/**
 * Checks if the system requirements are met
 *
 * @return bool True if system requirements are met, false if not
 */
function wpps_requirements_met() {
    global $wp_version;
    
	if ( version_compare( PHP_VERSION, WPPS_REQUIRED_PHP_VERSION, '<' ) ) {
		return false;
	}
	if ( version_compare( $wp_version, WPPS_REQUIRED_WP_VERSION, '<' ) ) {
		return false;
    }
    
	return true;
}

/**
 * Prints an error that the system requirements weren't met.
 */
function wpps_requirements_error() {
	global $wp_version;
	require_once( dirname( __FILE__ ) . '/views/requirements-error.php' );
}

/*
 * Check requirements and load main class
 * The main program needs to be in a separate file that only gets loaded if the plugin requirements are met. Otherwise older PHP installations could crash when trying to parse it.
 */
if(wpps_requirements_met()) {
    require_once(__DIR__ . '/controllers/class.catalog.php');
    require_once(__DIR__ . '/controllers/class.shortcode.php');

    global $ce_shortcodes;
    global $ce_main;

    if(class_exists('CEMain')) {
        $ce_main = new CEMain();
    }

} else {
    add_action( 'admin_notices', 'wpps_requirements_error' );
}

