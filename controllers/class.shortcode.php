<?php

if(!class_exists('CEShortcode')) {

    class CEShortcode {

        /**
         * Add add_action, add_shortcode, add_filter functions
         * For the callback name, use this: array($this, '<function name>')
         */
        public function __construct() {
            add_action( 'init', array($this, 'addShortcode' ));
        }

        function addShortcode() {
            add_shortcode( 'ce-advanced-search', array($this, 'ceSample' ));
            add_shortcode( 'ce-search', array($this, 'ceSearchBar' ));
            add_shortcode( 'ce-search-result', array($this, 'ceSearchResult' ));
            add_shortcode( 'celoaddb', array($this, 'loadPostFromDb' ));
        }

        function ceSample($atts = [], $content = null, $tag = '') {
            // SETUP ATTS
            $atts = array_change_key_case((array)$atts, CASE_LOWER);

            $ceSampleAtts = shortcode_atts([
                'title' => 'WordPress.org',
            ], $atts, $tag);

            
            global $wpdb;

            $makeResults = $wpdb->get_col('SELECT make_name FROM ' . $wpdb->prefix . 'list_make' );
            $modelResults = $wpdb->get_col('SELECT model_name FROM ' . $wpdb->prefix . 'list_model' );
            $engineResults = $wpdb->get_col('SELECT engine_name FROM ' . $wpdb->prefix . 'list_engine' );
            $categoryResults = $wpdb->get_col('SELECT category_name FROM ' . $wpdb->prefix . 'list_category' );
            
            $year = '';      
            $make = '';
            $model = '';            
            $engine = '';
            $category = '';

            for($i = 2017; $i >= 1950; $i--) {
                $year .= '<option>' . $i . '</option>';
            }
            
            foreach ($makeResults as $result) {
                $make .= '<option>' . $result . '</option>';                
            }

            foreach ($modelResults as $result) {
                $model .= '<option>' . $result . '</option>';                
            }

            foreach ($engineResults as $result) {
                $engine .= '<option>' . $result . '</option>';                
            }

            foreach ($categoryResults as $result) {
                $category .= '<option>' . $result . '</option>';                
            }

            $o = '';
            $o .= '<div class="sample-box">';
            $o .= '<h2>' . esc_html__($ceSampleAtts['title']) . '</h2><hr/>';       

            $o .= '<p style="margin: 8px">';

            $o .= '<select class="ce-dropdown"><option>Year</option>' . $year . '</select>';
            $o .= '<select class="ce-dropdown"><option>Make</option>' . $make . '</select>';
            $o .= '<select class="ce-dropdown"><option>Model</option>' . $model . '</select>';
            $o .= '<select class="ce-dropdown"><option>Engine</option>' . $engine . '</select>';
            $o .= '<select class="ce-dropdown"><option>Category</option>' . $category . '</select>';

            $o .= '</p>';    
            $o .= '</div>';

            return $o;
        }

        function ceSearchBar($atts = [], $content = null, $tag = '') {
            $o = '<div class="ce-search">';
            $o .= '<input id="search-part-input" type="text" class="ce-search-bar" placeholder="Find your parts here" />';
            $o .= '</div>';
            return $o;
        }

        function ceSearchResult($atts = [], $content = null, $tag = '') {            
            if(isset($_GET["query"]) && isset($_GET["year"]) && isset($_GET["make"]) && isset($_GET["model"]) && isset($_GET["engine"]) && isset($_GET["category"])) {
                global $wpdb;
                $tableMain = $wpdb->prefix . 'ce_parts';
                $tableLbg = $wpdb->prefix . 'list_lbg';
                $tableInterchanges = $wpdb->prefix . 'ce_interchanges';
                $tableOems = $wpdb->prefix . 'ce_oems';
                $tableYear = $wpdb->prefix . 'list_year';
                $tableMake = $wpdb->prefix . 'list_make';
                $tableModel = $wpdb->prefix . 'list_model';
                $tableEngine = $wpdb->prefix . 'list_engine';
                $o = '';

                $results = $wpdb->get_results(
                    '
                        SELECT lbg.part_name, inter.*, oem.*, year.display_year, make.make_name, model.model_name, engine.engine_name
                        FROM ' . $tableMain . ' AS main
                        INNER JOIN ' . $tableLbg . ' AS lbg
                        ON main.lbg_number = lbg.id
                        INNER JOIN ' . $tableInterchanges . ' AS inter
                        ON main.interchange_id = inter.id
                        INNER JOIN ' . $tableOems . ' AS oem
                        ON main.oem_id = oem.id
                        INNER JOIN ' . $tableYear . ' AS year
                        ON main.year_id = year.id
                        INNER JOIN ' . $tableMake . ' AS make
                        ON main.make_id = make.id
                        INNER JOIN ' . $tableModel . ' AS model
                        ON main.model_id = model.id
                        INNER JOIN ' . $tableEngine . ' AS engine
                        ON main.engine_id = engine.id
                        WHERE main.id LIKE "%' . $wpdb->esc_like($_GET["query"]) . '%"
                    '
                );

                foreach ($results as $result) {
                    $o .= '<p>';
                    $o .= '<b>LBG Part Number:</b> ' . $result->part_name;     
                    $o .= '<br>';

                    if($result->gates_aus !== NULL) {
                        $o .= '<b>Gates AUS:</b> ' . $result->gates_aus;    
                        $o .= '<br>';
                    }

                    if($result->gates_usa !== NULL) {
                        $o .= '<b>Gates USA:</b> ' . $result->gates_usa;    
                        $o .= '<br>';
                    }

                    if($result->dayco_aus !== NULL) {
                        $o .= '<b>Dayco AUS:</b> ' . $result->dayco_aus;    
                        $o .= '<br>';
                    }

                    if($result->continental !== NULL) {
                        $o .= '<b>Continental:</b> ' . $result->continental;    
                        $o .= '<br>';
                    }

                    if($result->other !== NULL) {
                        $o .= '<b>Other:</b> ' . $result->other;    
                        $o .= '<br>';
                    }

                    if($result->oem1 !== NULL) {
                        $o .= '<b>OEM Equivalent:</b> ' . $result->oem1 . ($result->oem2 !== NULL && trim($result->oem2) !== '' ? ', ' . $result->oem2 : '');
                        $o .= '<br>';
                    }

                    if($result->display_year !== NULL) {
                        $o .= '<b>Vehicle Fitment: </b>' . $result->display_year . ' ' .
                            ($result->make_name !== NULL && trim($result->make_name) !== '' ? $result->make_name : '') . ' ' .
                            ($result->model_name !== NULL && trim($result->model_name) !== '' ? $result->model_name : '') . ' ' .
                            ($result->engine_name !== NULL && trim($result->engine_name) !== '' ? $result->engine_name : '');
                        $o .= '<br>';
                    }


                    $o .= '</p>';
                }

                return $o;
            } else {
                return "GOT NOTHING";
            }
        }

        function loadPostFromDb($atts = [], $content = null, $tag = '') {
            global $wpdb;
            $results = $wpdb->get_col('SELECT post_title FROM ' . $wpdb->prefix . 'posts' );

            $vae = '';

            foreach ($results as $result) {
                $vae .= $result;
                
            }

            return $vae;

        }

        function loadPart($atts = [], $content = null, $tag = '') {
            global $wpdb;
            $results = $wpdb->get_col('SELECT  FROM ' . $wpdb->prefix . 'posts' );

            $vae = '';

            foreach ($results as $result) {
                $vae .= $result;
                
            }

            return $vae;

        }
    }
}