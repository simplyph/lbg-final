<?php

if(!class_exists('CEMain')) {

    /**
     * Main Controller
     */
    class CEMain {
        public function __construct() {
            register_activation_hook( plugin_dir_path( __DIR__ ) . '/catalog-editor.php', array('CEMain', 'createTables') );            
            //add_action( 'admin_enqueue_scripts', array($this, 'loadJSExcel') );
            add_action( 'wp_enqueue_scripts', array($this, 'loadResouces') );
            add_action( 'admin_menu', array($this, 'setupMenu') );
            $ceShortcode = new CEShortcode();
        }

        function loadResouces() {

            wp_register_style( 'ce_main', plugins_url( 'views/css/main.css', dirname(__FILE__) ) );
            wp_enqueue_style( 'ce_main' );
        }

        function modifyJqueryVersion() {
            wp_enqueue_script('jquerya', 'http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(), '3.2.1'
            , true);

        }

        public static function createTables() {
            global $wpdb;

            $charset_collate = $wpdb->get_charset_collate();
            $table_name = $wpdb->prefix . 'sample_table';

            $sql = "CREATE TABLE " . $table_name . " (
                    id MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
                    part_name VARCHAR(50) NOT NULL,
                    PRIMARY KEY  (id)
            ) ". $charset_collate .";";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
        

        function setupMenu() {
            add_menu_page( 
                'Catalog Editor', 
                'Catalog Editor', 
                'manage_options',
                'catalog_editor_main',
                array($this, 'loadMainMenuPage'),                
                '', 150 );

            add_submenu_page( 
                'catalog_editor_main', 
                'Configurations', 
                'Configurations', 
                'manage_options', 
                'catalog_editor_configurations', 
                array($this, 'loadConfigurationsPage') );
        }

        function loadMainMenuPage() {
            include_once( plugin_dir_path( __DIR__ ) . '/views/catalog/catalog_editor_main.php' );
        }

        function loadConfigurationsPage() {
            include_once( plugin_dir_path( __DIR__ ) . '/views/catalog/catalog_editor_configuration.php' );
        }

    }
}